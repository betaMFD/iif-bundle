<?php

namespace BetaMFD\IifBundle;

class TransactionRow
{
    /**
     * @var string
     */
    protected $bang;

    /**
     * @var string
     */
    protected $trnsId = '';

    /**
     * @var string
     */
    protected $splId = '';

    /**
     * @var string
     */
    protected $trnsType = '';

    /**
     * @var string
     */
    protected $date;

    /**
     * @var string
     */
    protected $accnt;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $class = '';

    /**
     * @var string
     */
    protected $amount;

    /**
     * @var string
     */
    protected $docNum = '';

    /**
     * @var string
     */
    protected $memo = '';

    /**
     * @var string
     */
    protected $clear = '';

    /**
     * @var integer
     */
    protected $qnty;

    /**
     * @var string
     */
    protected $price;

    /**
     * @var string
     */
    protected $invItem;

    /**
     * @var string
     */
    protected $toPrint;

    /**
     * @var string
     */
    protected $nameIsTaxable;

    /**
     * @var string
     */
    protected $dueDate;

    /**
     * @var string
     */
    protected $terms;

    /**
     * @var string
     */
    protected $paid;

    /**
     * @var string
     */
    protected $payMeth;

    /**
     * @var string
     */
    protected $taxable;

    /**
     * @var string
     */
    protected $valAdj;

    /**
     * @var string
     */
    protected $reimbExp;

    /**
     * @var string
     */
    protected $shipVia;

    /**
     * @var string
     */
    protected $shipDate;

    /**
     * @var string
     */
    protected $other1;

    /**
     * @var string
     */
    protected $poNum;

    /**
     * @var string
     */
    protected $toSend;

    /**
     * @var string
     */
    protected $isAJE;

    /**
     * @var string
     */
    protected $extra;

    public function __construct(array $values)
    {
        foreach ($values as $key => $value) {
            if ($value instanceof \DateTime) {
                $this->$key = $value->format('Y-m-d');
            } else {
                if (!is_array($value)) {
                    $this->$key = $value;
                } else {
                    throw new \Exception("$key can't be an array but an array was given!");
                }
            }
        }
    }

    public function getText($columns, $colCount)
    {
        $row = $this->bang . "\t";
        $key = '!' . $this->bang;
        $i = 0;
        $array = [];
        foreach ($columns[$key] as $col) {
            $i++;
            $array[$col] = $this->$col;
        }
        $row .= implode("\t", $array);
        if ($i < $colCount) {
            for ($j = $i; $j < $colCount; $j++) {
                $row .= "\t";
            }
        }
        return $row . "\n";
    }

    public function isTransaction() {
        return $this->bang == 'TRNS';
    }

    public function isSplit() {
        return $this->bang == 'SPL';
    }

    /**
     * Get the value of Amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Get the value of Bang
     *
     * @return string
     */
    public function getBang()
    {
        return $this->bang;
    }

    /**
     * Get the value of Trns Id
     *
     * @return string
     */
    public function getTrnsId()
    {
        return $this->trnsId;
    }

    /**
     * Get the value of Spl Id
     *
     * @return string
     */
    public function getSplId()
    {
        return $this->splId;
    }

    /**
     * Get the value of Trns Type
     *
     * @return string
     */
    public function getTrnsType()
    {
        return $this->trnsType;
    }

    /**
     * Get the value of Date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get the value of Accnt
     *
     * @return string
     */
    public function getAccnt()
    {
        return $this->accnt;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Get the value of Doc Num
     *
     * @return string
     */
    public function getDocNum()
    {
        return $this->docNum;
    }

    /**
     * Get the value of Memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Get the value of Clear
     *
     * @return string
     */
    public function getClear()
    {
        return $this->clear;
    }

    /**
     * Get the value of Qnty
     *
     * @return integer
     */
    public function getQnty()
    {
        return $this->qnty;
    }

    /**
     * Get the value of Price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get the value of Inv Item
     *
     * @return string
     */
    public function getInvItem()
    {
        return $this->invItem;
    }

    /**
     * Get the value of To Print
     *
     * @return string
     */
    public function getToPrint()
    {
        return $this->toPrint;
    }

    /**
     * Get the value of Name Is Taxable
     *
     * @return string
     */
    public function getNameIsTaxable()
    {
        return $this->nameIsTaxable;
    }

    /**
     * Get the value of Due Date
     *
     * @return string
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Get the value of Terms
     *
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Get the value of Paid
     *
     * @return string
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Get the value of Pay Meth
     *
     * @return string
     */
    public function getPayMeth()
    {
        return $this->payMeth;
    }

    /**
     * Get the value of Taxable
     *
     * @return string
     */
    public function getTaxable()
    {
        return $this->taxable;
    }

    /**
     * Get the value of Val Adj
     *
     * @return string
     */
    public function getValAdj()
    {
        return $this->valAdj;
    }

    /**
     * Get the value of Reimb Exp
     *
     * @return string
     */
    public function getReimbExp()
    {
        return $this->reimbExp;
    }

    /**
     * Get the value of Ship Via
     *
     * @return string
     */
    public function getShipVia()
    {
        return $this->shipVia;
    }

    /**
     * Get the value of Ship Date
     *
     * @return string
     */
    public function getShipDate()
    {
        return $this->shipDate;
    }

    /**
     * Get the value of Other
     *
     * @return string
     */
    public function getOther1()
    {
        return $this->other1;
    }

    /**
     * Get the value of Po Num
     *
     * @return string
     */
    public function getPoNum()
    {
        return $this->poNum;
    }

    /**
     * Get the value of To Send
     *
     * @return string
     */
    public function getToSend()
    {
        return $this->toSend;
    }

    /**
     * Get the value of Is
     *
     * @return string
     */
    public function getIsAJE()
    {
        return $this->isAJE;
    }

    /**
     * Get the value of Extra
     *
     * @return string
     */
    public function getExtra()
    {
        return $this->extra;
    }
}
