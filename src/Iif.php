<?php

namespace BetaMFD\IifBundle;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Iif
{
    /**
     * @var array
     */
    protected $groups = [];

    public function getFile($filename)
    {
        $response = new Response ($this->getFileContent());
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }

    public function getFileContent(): string
    {
        $colCount = $this->getColCount();
        $text = '';
        foreach ($this->groups as $g) {
            $text .= $g->getFileContent($colCount);
        }
        return $text;
    }

    /**
     * Gets a column count for each group of transactions and returns the highest count
     * @return integer $colCount
     */
    public function getColCount(): int
    {
        $colCount = 0;
        foreach ($this->groups as $g) {
            $count = $g->getColCount();
            //keep the larger number
            $colCount = $colCount < $count ? $count : $colCount;
        }
        return $colCount;
    }

    public function getNewJournal()
    {
        $j = new \BetaMFD\IifBundle\Journal\Journal();
        $this->groups[] = $j;
        return $j;
    }

    public function getNewInvoice()
    {
        $i = new \BetaMFD\IifBundle\Invoice\Invoice();
        $this->groups[] = $i;
        return $i;
    }

    public function getNewBill()
    {
        $i = new \BetaMFD\IifBundle\Bill\Bill();
        $this->groups[] = $i;
        return $i;
    }

    public function getNewCheck()
    {
        $i = new \BetaMFD\IifBundle\Check\Check();
        $this->groups[] = $i;
        return $i;
    }

    public function getNewCreditCard()
    {
        $i = new \BetaMFD\IifBundle\CreditCard\CreditCard();
        $this->groups[] = $i;
        return $i;
    }

    public function getNewCreditCardRefund()
    {
        $i = new \BetaMFD\IifBundle\CreditCard\CreditCardRefund();
        $this->groups[] = $i;
        return $i;
    }
}
